﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class PlayerScript : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed;
    private bool face_left = false;
    private bool jump = false;
    private Animator anim;
    public GameObject clue;
    private bool dead;
    private bool can_warp, is_warping;
    GameManager gm;
    public Vector3 checkpoint;
    public float jumpforce;
    public GameObject Teleporter;
    public string nextScene;
    public UnityEvent OnJump, OnDead, OnWarp;
    public string thisscene;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        gm = FindObjectOfType<GameManager>();
        can_warp = false;
        is_warping = false;
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine("restart");
        }

        if (Input.GetKeyDown(KeyCode.F) && gm.cooldown == false && can_warp)
        {
            StartCoroutine("travel");
            OnWarp.Invoke();
            
        }

        if (Input.GetKey(KeyCode.D)&& !dead && !is_warping)
        {
            transform.Translate(Vector2.left * -speed * Time.deltaTime);
            face_left = false;
            
            if (!jump)
            {
                anim.SetInteger("state", 1);
            }
            
        }

        if (Input.GetKey(KeyCode.A) && !dead && !is_warping)
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
            face_left = true;
            if (!jump)
            {
                anim.SetInteger("state", 1);
            }
            
        }

        if (Input.GetKeyDown(KeyCode.Space) && !jump && !dead && !is_warping)
        {
            jump = true;
            rb.AddForce(new Vector2(0, jumpforce));
            
            anim.SetInteger("state", 2);
            OnJump.Invoke();
        }

        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A))
        {
            if (!jump)
            {
                anim.SetInteger("state", 0);
            }

        }

        if (face_left == false)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }

        if (dead)
        {
            anim.SetInteger("state", 3);
            StartCoroutine("respawn");
            
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        

        if (collision.gameObject.CompareTag("ground"))
        {
            //anim.SetInteger("state", 0);
            jump = false;
        }

        if (collision.gameObject.CompareTag("enemy"))
        {
            Destroy(collision.gameObject);

        }

        if (collision.gameObject.CompareTag("enemycantkilled"))
        {
            dead = true;
            OnDead.Invoke();
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("obstacle"))
        {
            OnDead.Invoke();
            dead = true;
        }

        if (collision.gameObject.CompareTag("warp"))
        {
            can_warp = true;
            checkpoint = gameObject.transform.position;
        }

        if (collision.gameObject.name=="teleporter")
        {
            //can_warp = true;
            checkpoint = gameObject.transform.position;
            StartCoroutine("tele");
        }

        if (collision.gameObject.name == "finish")
        {
            
            StartCoroutine("changescene");
        }

        if (collision.gameObject.CompareTag("checkpoint"))
        {
            
            checkpoint = gameObject.transform.position;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("warp"))
        {
            can_warp = false;
            
        }
    }

    IEnumerator travel()
    {
        gm.cooldown = true;
        gm.transisi.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(1);
        gm.ChangeTimeline();
        is_warping = true;
        yield return new WaitForSeconds(1);
        gm.cooldown = false;
        is_warping = false;
    }

    IEnumerator respawn()
    {
        yield return new WaitForSeconds(1);
        gm.transisi.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(1);       
        gameObject.transform.position = checkpoint;
        anim.SetInteger("state", 0);
        dead = false;
    }

    IEnumerator tele()
    {
        
        gm.transisi.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(1);
        gm.ChangeTimeline();
        gameObject.transform.position = Teleporter.transform.position;

    }

    IEnumerator changescene()
    {
        yield return new WaitForSeconds(1);
        gm.transisi.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(nextScene);
    }

    IEnumerator restart()
    {
        yield return new WaitForSeconds(1);
        gm.transisi.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(thisscene);
    }
}
