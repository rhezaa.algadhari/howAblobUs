﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    public Vector3 offset;
    public int batas_kanan, batas_kiri, yPast, yFuture;
    GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame    

    void Update()
    {
        // Camera follows the player with specified offset position
        if (gm.past == true)
        {
            transform.position = new Vector3(player.transform.position.x, yPast, gameObject.transform.position.z);
        }

        if (gm.past == false)
        {
            transform.position = new Vector3(player.transform.position.x, yFuture, gameObject.transform.position.z);
        }

       
        

        if(gameObject.transform.position.x < batas_kiri)
        {
            gameObject.transform.position = new Vector3(batas_kiri, gameObject.transform.position.y, gameObject.transform.position.z);
        }

        if (gameObject.transform.position.x > batas_kanan)
        {
            gameObject.transform.position = new Vector3(batas_kanan, gameObject.transform.position.y, gameObject.transform.position.z);
        }
    }
}
