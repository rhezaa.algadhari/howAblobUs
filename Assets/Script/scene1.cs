﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scene1 : GameManager
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        destroyOne(objectPast[1], objectFuture[1]);
        destroyOne(objectFuture[0], objectPast[0]);
        destroyOne(objectPast[6], objectFuture[6]);


        for (int i = 2; i < 6; i++)
        {
            objectFuture[i].transform.position = new Vector3(objectPast[i].transform.position.x, objectFuture[i].transform.position.y, objectFuture[i].transform.position.z);
        }

        if (past == true)
        {
            timeline.text = "Past";

        }
        else
        {
            timeline.text = "Future";
        }
    }
}
