﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    public Transform patrolstart, patrolend;
    public float speed;
    public bool gopatrol;
    SpriteRenderer sr;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        anim.SetInteger("state", 1);
    }

    // Update is called once per frame
    void Update()
    {
        patroling();

        if (this.gameObject.transform.position.x == patrolstart.transform.position.x)
        {
            gopatrol = true;
            sr.flipX = true;
            
        }

        if (this.gameObject.transform.position.x ==patrolend.transform.position.x)
        {
            gopatrol = false;
            sr.flipX = false;

        }

    }

    public void patroling()
    {
        switch (gopatrol)
        {
            case true:
                    transform.position = Vector2.MoveTowards(this.transform.position, patrolend.transform.position, speed * Time.deltaTime);
                break;

            case false:
                    transform.position = Vector2.MoveTowards(this.transform.position, patrolstart.transform.position, speed * Time.deltaTime);
                break;
        }
    }
}
