﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public bool past;
    public GameObject[] objectPast;
    public GameObject[] objectFuture;
    public GameObject[] warppoint;
    public Text timeline;
    public GameObject transisi;
    public bool cooldown;
    public int ychangescene;
    
    public GameObject player;
    
    // Start is called before the first frame update
    void Start()
    {
       
        warppoint = GameObject.FindGameObjectsWithTag("warp");
        past = false;
      
       
               
        
    }

    // Update is called once per frame
    void Update()
    {
               

        
     
    }

    public void ChangeTimeline()
    {
        switch (past)
        {
            case true:
                player.gameObject.transform.position = new Vector2(player.gameObject.transform.position.x, player.gameObject.transform.position.y + ychangescene);
                past = false;
                break;

            case false:
                player.gameObject.transform.position = new Vector2(player.gameObject.transform.position.x, player.gameObject.transform.position.y - ychangescene);

                past = true;
                break;
        }
    }

   

    public void destroyOne(GameObject object1, GameObject objectAlter)
    {
        if (!object1)
        {
            Destroy(objectAlter);
        }
    }

    public void moveOne(GameObject a, GameObject b)
    {
        b.transform.position = new Vector3(a.transform.position.x, b.transform.position.y, b.transform.position.z);
    }
}
