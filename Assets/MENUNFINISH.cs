﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MENUNFINISH : MonoBehaviour
{
    public string nextscene;
    public GameObject transisi;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            StartCoroutine("nextStage");
        }
    }

    IEnumerator nextStage()
    {
        transisi.GetComponent<Animation>().Play();
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(nextscene);
    }
}
