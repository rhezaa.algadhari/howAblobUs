﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scene2 : GameManager
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        destroyOne(objectFuture[0], objectPast[0]);

        for (int i = 1; i < 6; i++)
        {
            objectFuture[i].transform.position = new Vector3(objectPast[i].transform.position.x, objectFuture[i].transform.position.y, objectFuture[i].transform.position.z);
        }

        if (past == true)
        {
            timeline.text = "Past";

        }
        else
        {
            timeline.text = "Future";
        }
    }
}
